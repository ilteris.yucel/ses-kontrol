

var calculateScreenSizes = function(){
    var screen = {
      beginX: 0,
      beginY: 0,
      width: window.innerWidth,
      height: window.innerHeight,
    };
    return screen;
  }
  var calculateStepSize = function(screen){
    var width = screen['width'];
    var height = screen['height'];
    var big = width > height ? width : height;
    var small = width > height ? height : width;
    var step = small / 18;
    if(step * 30 > big){
      var diff = (step * 30) - big;
      step = step - (diff / 30);
    }
    return step;
  }

  var createMainDiv = function(){
    var screen = calculateScreenSizes();
    var mainDiv = document.createElement('div');
    mainDiv.setAttribute('id', 'mainDiv');
    mainDiv.style.width = screen.width + 'px';
    mainDiv.style.height = screen.height + 'px';
    mainDiv.style.margin = '0px';
    mainDiv.style.padding = '0px';
    mainDiv.style.boxSizing = 'border-box';
    mainDiv.style.border = '1px solid #EEE2DC';
    mainDiv.style.backgroundColor = "#EEE2DC";
    document.body.appendChild(mainDiv);
  }

  var createUserButton = function(){
      var screen = calculateScreenSizes();
      var step = calculateStepSize(screen);
      var parentDiv = document.getElementById('mainDiv');

      var userButtonDiv = document.createElement('div');
      userButtonDiv.setAttribute('id', 'userButtonDiv');
      userButtonDiv.style.width = '100%';
      userButtonDiv.style.height = 3 * step + 'px';
      userButtonDiv.style.boxSizing = 'border-box';
      parentDiv.appendChild(userButtonDiv);

      var selectUserButton = document.createElement('button');
      var addUserButton = document.createElement('button');
      var listUserButton = document.createElement('button');

      selectUserButton.setAttribute('id', 'selectUser');
      addUserButton.setAttribute('id', 'addUser');
      listUserButton.setAttribute('id', 'listUser');

      selectUserButton.style.width = '31%';
      addUserButton.style.width = '31%';
      listUserButton.style.width = '31%';

      addUserButton.style.height = '99%';
      selectUserButton.style.height = '99%';
      listUserButton.style.height = '99%';

      selectUserButton.style.boxSizing = 'border-box';
      addUserButton.style.boxSizing = 'border-box';
      listUserButton.style.boxSizing = 'border-box';

      selectUserButton.style.marginTop = '1%';
      addUserButton.style.marginTop = '1%';
      listUserButton.style.marginTop = '1%';

      selectUserButton.style.marginLeft = '1%';
      selectUserButton.style.marginRight = '1%';

      addUserButton.style.marginLeft = '1%';
      addUserButton.style.marginRight = '1%';

      listUserButton.style.marginRight = '1%';
      listUserButton.style.marginLeft = '1%';

      selectUserButton.style.float = 'left';
      listUserButton.style.float = 'left';
      addUserButton.style.float = 'left';

      selectUserButton.innerHTML = 'SELECT PATIENT';
      addUserButton.innerHTML = 'ADD PATIENT';
      listUserButton.innerHTML = 'LIST PATIENTS';

      addUserButton.addEventListener('click', dealClick);
      selectUserButton.addEventListener('click', dealClick);
      listUserButton.addEventListener('click', dealClick);

      userButtonDiv.appendChild(selectUserButton);
      userButtonDiv.appendChild(addUserButton);
      userButtonDiv.appendChild(listUserButton);
  }
  var createTabDiv = function(){
      var screen = calculateScreenSizes();
      var step = calculateStepSize(screen);
      var parentDiv = document.getElementById('mainDiv');
      var tabDiv = document.createElement('div');
      tabDiv.setAttribute('id', 'tabDiv');
      tabDiv.style.width = '98%',
      tabDiv.style.height = 10 * step + 'px';
      tabDiv.style.border = '1px solid black';
      tabDiv.style.boxSizing = 'border-box';
      tabDiv.style.marginLeft = '1%';
      tabDiv.style.marginRight = '1%';
      tabDiv.style.marginTop = step + 'px';
      parentDiv.appendChild(tabDiv);
  }
  var createUserAddTab = function(){
    var screen = calculateScreenSizes();
    var step = calculateStepSize(screen);
    var parentDiv = document.getElementById('mainDiv');
    var tab = document.createElement('div');
    var userForm = document.createElement('form');

    //userForm.setAttribute('action', 'https://young-ravine-51100.herokuapp.com/add-patient');
    //userForm.setAttribute('method', 'post');

    tab.setAttribute('id', 'userForm');
    tab.style.width = '98%';
    tab.style.margin = '2% 1%';
    tab.style.backgroundColor = "#f2f2f2";
    tab.style.boxSizing = 'border-box';
    tab.style.borderRadius = step / 4 + 'px';
    tab.style.padding = step + 'px';
    
    var nameLabel = document.createElement('label');
    nameLabel.innerHTML = 'First Name';
    nameLabel.setAttribute('for', 'nameInput');
    userForm.appendChild(nameLabel);

    var nameInput = document.createElement('input');
    nameInput.setAttribute('type', 'text');
    nameInput.setAttribute('id', 'nameInput');
    nameInput.setAttribute('name', 'nameInput');
    nameInput.setAttribute('placeholder', 'Type Patient Name >');
    nameInput.style.display = 'block';
    nameInput.style.width = '100%';
    nameInput.style.borderRadius = step / 4 + 'px';
    nameInput.style.padding = "8px 16px";
    nameInput.style.margin = "8px 0";
    userForm.appendChild(nameInput);

    var sNameLabel = document.createElement('label');
    sNameLabel.innerHTML = 'Last Name';
    sNameLabel.setAttribute('for', 'sNameInput');
    userForm.appendChild(sNameLabel);

    var sNameInput = document.createElement('input');
    sNameInput.setAttribute('type', 'text');
    sNameInput.setAttribute('id', 'sNameInput');
    sNameInput.setAttribute('name', 'sNameInput');
    sNameInput.setAttribute('placeholder', 'Type Patient Surname >')
    sNameInput.style.display = 'block';
    sNameInput.style.width = '100%';
    sNameInput.style.borderRadius = step / 4 + 'px';
    sNameInput.style.padding = "8px 16px";
    sNameInput.style.margin = "8px 0px";
    userForm.appendChild(sNameInput);

    var submitInput = document.createElement('button');
    submitInput.setAttribute('type', 'button');
    submitInput.setAttribute('id', 'submitInput');
    submitInput.setAttribute('name', 'submit');
    //submitInput.setAttribute('value', 'ADD PATIENT');
    submitInput.textContent = 'ADD PATIENT';
    submitInput.style.display = 'block';
    submitInput.style.width = "100%"
    submitInput.style.margin = "16px 0px";
    userForm.appendChild(submitInput);
    tab.appendChild(userForm);

    tab.style.display = 'none';
    parentDiv.appendChild(tab);
  }

  var listUserTab = function(){
    var screen = calculateScreenSizes();
    var step = calculateStepSize(screen);
    var parentDiv = document.getElementById('mainDiv');
    var tab = document.createElement('div');

    parentDiv.appendChild(tab);

    tab.setAttribute('id', 'listUserTab');
    tab.style.width = '98%';
    tab.style.margin = '2% 1%';
    tab.style.height = '40%';
    tab.style.backgroundColor = "#f2f2f2";
    tab.style.boxSizing = 'border-box';
    tab.style.borderRadius = step / 4 + 'px';
    tab.style.padding = '2% 4%';
    tab.style.display = 'none';
    tab.style.overflowY = 'auto';

    var search = document.createElement('input');
    search.setAttribute('id', 'userListSearch');
    search.setAttribute('placeholder', 'Patient Search >')
    search.style.width = '100%';
    search.style.height = '10%';
    search.style.padding = '4px 8px';
    search.style.boxSizing = 'border-box';
    search.style.marginBottom = '2%'
    search.addEventListener('input', dealPatientSearch);
    //search.style.borderRadius = step / 4 + 'px';
    tab.appendChild(search);

    var userTable = document.createElement('table');
    userTable.setAttribute('id', 'userTable');
    userTable.style.width = '100%';
    userTable.style.tableLayout = 'fixed';
    tab.appendChild(userTable);

    var userTableTHead = document.createElement('thead');
    userTableTHead.setAttribute('id', 'userTableHead');
    userTable.appendChild(userTableTHead);

    createRow(userTableTHead, -1, {'name': 'name', 'surname' : 'surname', 'test0':'test0', 'test1': 'test1'}, true);

    var userTableBody = document.createElement('tbody');
    userTableBody.setAttribute('id', 'userTableBody');
    userTable.appendChild(userTableBody);

    $.get('https://young-ravine-51100.herokuapp.com/get-all-patient', function(data, status){
      console.log(status);
      var colList = Object.values(data);
      console.log(colList);
      for(var i = 0; i < colList.length; i++){
        createRow(userTableBody, i, colList[i]);
      }
    })
  }
  var createRow = function(parent, rowId, colItems, head = false){
    var row = document.createElement('tr');
    row.setAttribute('id', 'row'+rowId);
    var selector = head ? 'th' : 'td';
    var iClass = head ? '' : 'item';
    row.setAttribute('class', iClass);
    var colList = Object.values(colItems);
    for(var i = 0; i < colList.length; i++){
      var col = document.createElement(selector);
      col.setAttribute('id', 'row'+rowId+'col'+i);
      col.style.textAlign = 'left';
      var val = colList[i] ? colList[i].toUpperCase() : 'EMPTY';
      col.innerHTML = val;
      row.appendChild(col);
    }
    parent.appendChild(row);
  }
  var dealClick = function(event){
    this.data = event.data;
    switch(this.id){
      case 'addUser':
        document.getElementById('userForm').style.display = 'block';
        document.getElementById('userSelect').style.display = 'none';
        document.getElementById('listUserTab').style.display ='none';
        return;
      case 'selectUser':
        document.getElementById('userForm').style.display = 'none';
        document.getElementById('userSelect').style.display = 'block';
        document.getElementById('listUserTab').style.display ='none';
        return;
      case 'listUser':
        document.getElementById('userForm').style.display = 'none';
        document.getElementById('userSelect').style.display = 'none';
        document.getElementById('listUserTab').style.display ='block';
        return;
      default:
          console.log('İnvalid Op');
          return;
    }
  }
  var patientList = function(){
    var screen = calculateScreenSizes();
    var step = calculateStepSize(screen);
    var parentDiv = document.getElementById('mainDiv');
    var tab = document.createElement('div');
    tab.setAttribute('id', 'userSelect');
    tab.style.width = "%98";
    tab.style.margin = "2% 1%";
    tab.style.backgroundColor = "#f2f2f2";
    tab.style.boxSizing = 'border-box';
    tab.style.borderRadius = step / 4 + 'px';
    tab.style.padding = step + 'px';
    tab.style.display = 'none';

    var input = document.createElement('input');
    input.setAttribute('id', 'selectInput');
    input.setAttribute('placeholder', 'Search Patient>');
    input.style.width = "100%";
    input.style.borderRadius = step / 4 + 'px';
    input.style.padding = "8px 16px";
    input.style.margin = "2% 0";
    input.addEventListener('input', searchInputDeal);
    tab.appendChild(input);
    $.get('https://young-ravine-51100.herokuapp.com/get-all-patient', function(data, status){
      if(status === 'success'){
        var select = document.createElement('select');
        select.setAttribute('id', 'patientSelect');
        select.style.width = '100%';
        select.selectedIndex = -1;
        createOptions(select, data);
        tab.appendChild(select);
        createTestOptions(tab);
        parentDiv.appendChild(tab);
      }else{
        console.log(status);
      }
    })
  }
  var createOptions = function(select, obj){
    var patientList = Object.keys(obj);
    var blank = document.createElement('option');
    blank.setAttribute('disabled', true);
    blank.setAttribute('selected', true);
    blank.setAttribute('blank', true);
    blank.setAttribute('hidden', true);
    blank.innerHTML = "<--- Select Patient --->";
    select.appendChild(blank);
    for(var i = 0; i < patientList.length; i++){
      var opt = document.createElement('option');
      opt.setAttribute('id', 'opt' + i);
      opt.setAttribute('userId', patientList[i]);
      opt.style.width = '100%';
      opt.innerHTML = obj[patientList[i]].name + " " + obj[patientList[i]].surname;
      select.appendChild(opt);
    }
  }
  var updateOptions = function(){
    var select = document.getElementById('patientSelect');
    while(select.options.length > 0){
      select.remove(0);
    }
    $.get('https://young-ravine-51100.herokuapp.com/get-all-patient', function(data, status){
      if(status === 'success' ){
        createOptions(select, data);
      }
    })
  }
  var wait = function(param, callback){
    if(param){
      console.log("PARAM NOT NULL");
      callback(param)
      return;
    }else{
      console.log("PARAM NULL");
      wait(param, callback);
    }
  }
  var createTestOptions = function(parentTab){
    var screen = calculateScreenSizes();
    var step = calculateStepSize(screen);
    //var parentDiv = document.getElementById('userSelect');
    var select = document.createElement('select');
    select.setAttribute('id', 'testSelect');
    select.style.width = '100%';
    select.style.marginTop ='2%';
    
    var blank = document.createElement('option');
    blank.setAttribute('disabled', true);
    blank.setAttribute('selected', true);
    blank.setAttribute('blank', true);
    blank.setAttribute('hidden', true);
    blank.innerHTML = "<--- Select Test --->";
    select.appendChild(blank);
    $.getJSON("./assets/jsons/testList.json", function(data){
      console.log(data);
      for(var i = 0; i < data.length; i++){
        var opt = document.createElement('option');
        opt.setAttribute('id', 'opt-test' + i);
        opt.style.width = '100%';
        opt.innerHTML = data[i];
        select.appendChild(opt);       
      }
      parentTab.appendChild(select);
      createStartButton(parentTab);
    })
  }

  var createStartButton = function(parentDiv){
    var button = document.createElement('button');
    button.setAttribute('id', 'createTestButton');
    button.style.width = "33%";
    button.style.marginTop = "2%";
    button.innerHTML = "START TEST";
    button.addEventListener('click', startTest);
    parentDiv.appendChild(button);
  }
  var render = function(){
      createMainDiv();
      createUserButton();
      //createTabDiv();
      createUserAddTab();
      patientList();
      listUserTab();
      submit();
      //createTestOptions();
  }
  var startTest = function(){
    var patientSelect = document.getElementById('patientSelect');
    var testSelect = document.getElementById('testSelect');
    var patient = patientSelect.value;
    var test = testSelect.value;
    var id = patientSelect.options[patientSelect.selectedIndex].getAttribute('userId');
    console.log(id);
    setTimeout(function(){
      window.location.assign('./html/quest.html?test='+test+'&patient='+patient+'&userId='+id);
    }, 1000);
    
  }
  var searchInputDeal = function(event){
    var select = document.getElementById('patientSelect');
    this.data = event.data;
    if(this.value === ""){
      select.value = select.options[0].text;
      return;
    }
    w3.filterHTML('#patientSelect', 'option', this.value);
    for(i = 0; i < select.options.length; i++){
      if(select.options[i].style.display !== 'none'){
        select.value = select.options[i].text;
      }
    }
  }

  var dealPatientSearch = function(event){
    this.data = event.data;
    w3.filterHTML('#userTable', '.item', this.value);
  }

  var submit = function(){

      $('#submitInput').click(function() {
        var nameInput = document.getElementById('nameInput');
        var sNameInput = document.getElementById('sNameInput');
        var name = nameInput.value;
        var surname = sNameInput.value;
        var tableBody = document.getElementById('userTableBody');
        console.log(name, surname);
        $.ajax({
            url: 'https://young-ravine-51100.herokuapp.com/add-patient',
            type: 'POST',
            data: {
                'name': name,
                'surname': surname
            },
            success: function(msg) {
                alert('Patient is added!');
                nameInput.value = '';
                sNameInput.value = '';
                updateOptions();
                createRow(tableBody, tableBody.children.length, {"name":name, "surname":surname, "test0":null, "test1":null});
            },
            error: function(){
              alert("Patient cannot be added");
            }               
        });
    });
  }
  window.onload = function(){
    render();
  }