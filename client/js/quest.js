var test = null;
var index = 0;
var answer = null;
var time = 0;
var total = 0;
var timeFlag = false;
var synth = window.speechSynthesis;
var voice = null;
var word = null;
var utterThis = null;
var intervalId = null;
var timeout = null;
var userId = null;
var imgPath = '../assets/images/';
var questPath = '../assets/jsons/quest/';
var testPath = '../assets/jsons/test/';

var calculateScreenSizes = function(){
  var screen = {
    beginX: 0,
    beginY: 0,
    width: window.innerWidth,
    height: window.innerHeight,
  };
  return screen;
}
var poulateVoices = function(){
  var voices = synth.getVoices();
  for(var i = 0; i < voices.length; i++){
    if(voices[i]['name'].toLowerCase().includes('turkish')){
      console.log(voices[i]);
      voice = voices[i];
    }
  }
  // console.log(voices[0]);
  //  var i = 0;
  //  while(!maleVoice || !femaleVoice){
  //    if(voices[i].name.toLowerCase().includes('turkish')
  //      && voices[i].name.toLowerCase().includes('anika')
  //      && !femaleVoice){
  //      //console.log(voices[i]);
  //      femaleVoice = voices[i];
  //    }
  //    if(voices[i].name.toLowerCase().includes('turkish')
  //      && voices[i].name.toLowerCase().includes('antonio')
  //      && !maleVoice){
  //      //console.log(voices[i]);
  //      maleVoice = voices[i];
  //    }
  //    i++;      
  //  }
  //  console.log(maleVoice);
  //  console.log(femaleVoice);
  //   for(var i = 0; i < voices.length; i++){
  //     if(voices[i].name.toLowerCase().includes('turkish')
  //       /*&& voices[i].name.toLowerCase().includes('female')*/){
  //       console.log(voices[i]);
  //    }
  //  }
}
var calculateStepSize = function(screen){
  var width = screen['width'];
  var height = screen['height'];
  var big = width > height ? width : height;
  var small = width > height ? height : width;
  var step = small / 18;
  if(step * 30 > big){
    var diff = (step * 30) - big;
    step = step - (diff / 30);
  }
  return step;
}

var createMainDiv = function(){
  var screen = calculateScreenSizes();
  var mainDiv = document.createElement('div');
  mainDiv.setAttribute('id', 'mainDiv');
  mainDiv.style.width = screen.width + 'px';
  mainDiv.style.height = screen.height + 'px';
  mainDiv.style.margin = '0px';
  mainDiv.style.padding = '0px';
  mainDiv.style.boxSizing = 'border-box';
  mainDiv.style.border = '1px solid #EEE2DC';
  mainDiv.style.backgroundColor = "#EEE2DC";
  document.body.appendChild(mainDiv);
}

var createPicturesDiv = function(){
  var screen = calculateScreenSizes();
  var step = calculateStepSize(screen);
  var size = step * 15;
  var parentDiv = document.getElementById('mainDiv');
  var picturesDiv = document.createElement('div');
  picturesDiv.setAttribute('id', 'picturesDiv');
  picturesDiv.style.width = size + 'px';
  picturesDiv.style.height = size + 'px';
  picturesDiv.style.marginTop = (screen.height - size) / 2 + 'px';
  picturesDiv.style.marginLeft = (screen.width - size) / 2 + 'px';
  picturesDiv.style.boxSizing = 'border-box';
  parentDiv.appendChild(picturesDiv);

  for(var i = 0; i < 3; i++){
    var row = document.createElement('div');
    row.setAttribute('class', 'row');
    row.setAttribute('id', 'row' + i);
    picturesDiv.appendChild(row);
  }
}
var createQuest = function(tag){
  var fullPath = questPath + tag + '.json';
  var screen = calculateScreenSizes();
  var step = calculateStepSize(screen);
  $.getJSON(fullPath, function(data){
    var optionsList = data.options;
    answer = extractAnswer(data.answer);
    createUtter(data.ses);
    speechSynthesis.speak(utterThis);
    for(var i = 0; i < optionsList.length; i++){
      createImage(optionsList[i], i, step);
    }
  }).fail(function(){
    console.log(fullPath);
    console.log('JSON can not be getted...');
  })
}
var createImage = function(path, id, step){
  var rowId = Math.floor(Math.floor(id / 3));
  var row = document.getElementById('row' + rowId);
  var div = document.createElement('div');
  var img = document.createElement('img');
  div.setAttribute('class', 'third');
  div.style.width = Math.floor(5 * step) + 'px';
  div.style.height = Math.floor(5 * step) + 'px';
  div.style.boxSizing = 'border-box';
  img.setAttribute('src', path);
  img.setAttribute('class', 'quest-img');
  img.setAttribute('id', 'opt' + id);
  img.style.boxSizing = 'border-box';
  img.style.padding = step / 8 + 'px';
  img.addEventListener('click', pictureClick);
  div.appendChild(img);
  row.appendChild(div);
}

var createButton = function(){
  var screen = calculateScreenSizes();
  var step = calculateStepSize(screen);
  var parentDiv = document.getElementById('mainDiv');
  var buttonDiv = document.createElement('div');
  var soundButton = document.createElement('button');
  var noiseButton = document.createElement('button');
  var soundAndNoiseButton = document.createElement('button');

  soundButton.innerHTML = 'SOUND';
  noiseButton.innerHTML = 'NOISE';
  soundAndNoiseButton.innerHTML = 'SO&NO';

  soundButton.setAttribute('class', 'third');
  noiseButton.setAttribute('class', 'third');
  soundAndNoiseButton.setAttribute('class', 'third');

  soundButton.setAttribute('id', 'sound');
  noiseButton.setAttribute('id', 'noise');
  soundAndNoiseButton.setAttribute('id', 'so&no');

  soundButton.style.width = 2.5 * step + 'px';
  noiseButton.style.width = 2.5 * step + 'px';
  soundAndNoiseButton.style.width = 2.5 * step + 'px';

  soundButton.addEventListener('click', buttonClick);
  noiseButton.addEventListener('click', buttonClick);
  soundAndNoiseButton.addEventListener('click', buttonClick);

  soundButton.style.backgroundColor = '#EDC7B7';
  noiseButton.style.backgroundColor = '#EDC7B7';
  soundAndNoiseButton.style.backgroundColor = '#EDC7B7';

  soundButton.style.color = '#123C69';
  noiseButton.style.color = '#123C69';
  soundAndNoiseButton.style.color = '#123C69';

  buttonDiv.style.width = 8 * step + 'px';
  buttonDiv.style.marginLeft = (screen.width - 8 * step) / 2 + 'px';
  buttonDiv.style.marginTop = step / 2 + 'px';

  mainDiv.appendChild(buttonDiv);
  
  buttonDiv.appendChild(soundButton);
  buttonDiv.appendChild(noiseButton);
  buttonDiv.appendChild(soundAndNoiseButton);
}

var createUtter = function(word){
  utterThis = new SpeechSynthesisUtterance(word);
  utterThis.voice = voice;
  utterThis.addEventListener('end', function(event){
    console.log('Utterance has finished being spoken after ' + event.elapsedTime + ' seconds.');
    if(!timeFlag){
      intervalId = setInterval(function(){
        time++;
      }, 1);
      timeFlag = true;
    }
  });
}

var render = function(){
  $('body').empty();
  createMainDiv();
  createPicturesDiv();
  createQuest(test[index]);
  createButton();
}

var buttonClick = function(event){
  this.data = event.data;
  switch (this.id){
    case 'sound':
      console.log('Sound playing...');
      speechSynthesis.speak(utterThis);
      return;
    case 'noise':
      console.log('Noise playing...');
      return;
    case 'so&no':
      speechSynthesis.speak(utterThis);
      console.log('Both of them playing...');
      return;
    default:
      console.log('Invalid event');
      return;
  }
}

var pictureClick = function(event){
  this.data = event.data;
  var userAnswer = extractAnswer(this.src);
  console.log(userAnswer);
  console.log(answer);
  if(!answer.localeCompare(userAnswer)){
    succes();
  }else{
    alert('FAIL');
  }
}

var extractAnswer = function(link){
  var answer = link.split('/').pop()
  return answer;
}

var succes = function(){
  clearInterval(intervalId);
  total += time;
  console.log(time);
  alert('Reaction Time : ' + millisToMinutesAndSeconds(time));
  time = 0;
  timeFlag = false;
  word = null;
  index++;
  if(test[index]){
      render();
    
  }else{
    finish();
  }
}

var finish = function(){
  console.log("FINISHED");
   $.ajax({
     url: 'https://young-ravine-51100.herokuapp.com/post-score',
     type: 'POST',
     data:{
       'userId': userId,
       'score': total / 1000 + 'sec'
     },
     success: function(msg) {
       alert("Total Reaction Time " + total);
       window.location.assign("../index.html");
     },
     error: function(XMLHttpRequest, textStatus, errorThrown){
       alert("Status: " + textStatus); alert("Error: " + errorThrown); 
     }         
 });

}

function firstPlay(){
  timeout = setTimeout(function(){
    if(utterThis){
      speechSynthesis.speak(utterThis);
      clearTimeout(timeout);
      return;
    }else{
      return firstPlay();
    }
  }, 1);
}
window.onload = function(){
  if(speechSynthesis.onvoiceschanged !== undefined){
    speechSynthesis.onvoiceschanged = poulateVoices();
  }
  var queryString = window.location.search;
  var urlParams = new URLSearchParams(queryString);
  var testName = urlParams.get('test');
  var patientName = urlParams.get('patient');
  userId = urlParams.get('userId');
  console.log(testName);
  console.log(patientName);
  console.log(userId);
  var testFullPath = testPath + '/' + testName + '.json';
  $.getJSON(testFullPath, function(data){
    test = data.tagList;
    index = 0;
    time = 0;
    render();
  }).fail(function(){
    console.log("Test cannot be getted...");
  });
}

