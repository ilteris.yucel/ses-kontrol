const express  =  require('express');
const path = require('path');
const app = express();
const cors = require('cors');
const fs = require('fs');
const port = 5001;

app.use(express.urlencoded());
app.use(express.json());
app.use(cors({
  origin: '*'
}));

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.post('/add-patient', (req, res) => {
  const rawData = fs.readFileSync('./files/patient.json');
  const patients = JSON.parse(rawData);
  const index = Object.keys(patients).length;
  console.log(index);
  patients[index] = {
    "name" : req.body.name,
    "surname" : req.body.surname,
    "test0" : null,
    "test1" : null
  }
  console.log(patients);
  fs.writeFileSync('./files/patient.json', JSON.stringify(patients));
  res.send('OK');
});

app.get('/get-all-patient', (req, res) => {
  const rawData = fs.readFileSync('./files/patient.json');
  const patients = JSON.parse(rawData);
  res.json(patients);
});

app.post('/post-score', (req, res) => {
  console.log("SCORE POSTING");
  const rawData = fs.readFileSync('./files/patient.json');
  let patients = JSON.parse(rawData);
  const id = req.body.userId;
  if(!patients[id]['test0']){
    patients[id]['test0'] = req.body.score;
  }else if(!patients[id]['test1']){
    patients[id]['test1'] = req.body.score;
  }else{
    atients[id]['test0'] = req.body.score;
  }
  fs.writeFileSync('./files/patient.json', JSON.stringify(patients));
  res.send('OK');   
});

app.listen(process.env.PORT || port, () => {
  console.log(`Example app listening on port ${port}!`)
});


app.use('/client', express.static(path.join(__dirname, 'client')));
app.use('/files', express.static(path.join(__dirname, 'files')));